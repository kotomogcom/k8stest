from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from graphframes import *

spark = (
    SparkSession.builder.appName("GraphApp")
    .config("spark.pyspark.python", "./environment/bin/python")
    .getOrCreate()
)
spark.sparkContext.addPyFile("/data/mount/modules/cube_udf.py")

from cube_udf import cube_udf

v = spark.createDataFrame(
    [
        ("a", "Alice", 34),
        ("b", "Bob", 36),
        ("c", "Charlie", 30),
        ("d", "David", 29),
        ("e", "Esther", 32),
        ("f", "Fanny", 36),
        ("g", "Gabby", 60),
    ],
    ["id", "name", "age"],
)

e = spark.createDataFrame(
    [
        ("a", "b", "friend"),
        ("b", "c", "follow"),
        ("c", "b", "follow"),
        ("f", "c", "follow"),
        ("e", "f", "follow"),
        ("e", "d", "friend"),
        ("d", "a", "friend"),
        ("a", "e", "friend"),
    ],
    ["src", "dst", "relationship"],
)

g = GraphFrame(v, e)
g.vertices.select(
    "age", "name", cube_udf(col("age")).alias("cubed_age")
).groupBy().mean("cubed_age").show()

spark.stop()
