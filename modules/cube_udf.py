import pandas as pd
from pyspark.sql.functions import pandas_udf
from pyspark.sql.types import LongType


def cube(s: pd.Series) -> pd.Series:
    return s * s


cube_udf = pandas_udf(cube, returnType=LongType())
