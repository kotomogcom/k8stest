#!/bin/bash

spark-submit \
  --master k8s://https://127.0.0.1:32779 \
  --deploy-mode cluster \
  --name graph-app \
  --jars local:///app/graphframes-0.8.2-spark3.2-s_2.12.jar \
  --files local:///app/graphframes-0.8.2-spark3.2-s_2.12.jar,local:///app/modules/cube_udf.py \
  --py-files local:///app/graphframes-0.8.2-spark3.2-s_2.12.jar,local:///app/modules/cube_udf.py \
  --archives local:///app/new_env.tar.gz#environment \
  --conf spark.executor.instances=2 \
  --conf spark.kubernetes.container.image=graph-app:v1.0.7 \
  --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark \
  --conf spark.pyspark.python=./environment/bin/python \
  --conf spark.kubernetes.context=minikube \
  --conf spark.kubernetes.namespace=default \
  --conf spark.kubernetes.executor.deleteOnTermination=false \
  local:///app/GraphApp.py