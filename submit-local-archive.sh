#!/bin/bash

spark-submit \
  --master local[4] \
  --name graph-app \
  --jars graphframes-0.8.2-spark3.2-s_2.12.jar \
  --py-files graphframes-0.8.2-spark3.2-s_2.12.jar,modules/cube_udf.py \
  --conf spark.pyspark.python=./mount/environment/bin/python \
  GraphAppLocal.py