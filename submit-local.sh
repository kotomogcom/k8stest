#!/bin/bash

spark-submit \
  --master local[4] \
  --jars graphframes-0.8.2-spark3.2-s_2.12.jar \
  --py-files graphframes-0.8.2-spark3.2-s_2.12.jar,mount/modules/cube_udf.py \
  ./GraphAppLocal.py