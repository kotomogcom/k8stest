#!/bin/bash

export SOURCE_DIR=$PWD/mount
export VOLUME_TYPE=hostPath
export VOLUME_NAME=conda-env
export MOUNT_PATH=/data/mount

spark-submit \
  --master k8s://https://127.0.0.1:32779 \
  --deploy-mode cluster \
  --name graph-app \
  --jars local:///app/graphframes-0.8.2-spark3.2-s_2.12.jar \
  --py-files local:///app/graphframes-0.8.2-spark3.2-s_2.12.jar \
  --conf spark.kubernetes.driver.volumes.$VOLUME_TYPE.$VOLUME_NAME.mount.path=$MOUNT_PATH \
  --conf spark.kubernetes.driver.volumes.$VOLUME_TYPE.$VOLUME_NAME.options.path=$MOUNT_PATH \
  --conf spark.kubernetes.executor.volumes.$VOLUME_TYPE.$VOLUME_NAME.mount.path=$MOUNT_PATH \
  --conf spark.kubernetes.executor.volumes.$VOLUME_TYPE.$VOLUME_NAME.options.path=$MOUNT_PATH \
  --conf spark.executor.instances=2 \
  --conf spark.kubernetes.container.image=graph-app:v1.0.10 \
  --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark \
  --conf spark.pyspark.python=$MOUNT_PATH/environment/bin/python \
  --conf spark.kubernetes.context=minikube \
  --conf spark.kubernetes.namespace=default \
  --conf spark.kubernetes.executor.deleteOnTermination=false \
  local:///app/GraphApp.py